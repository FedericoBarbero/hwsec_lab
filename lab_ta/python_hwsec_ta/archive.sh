#/usr/bin/env bash

#
# Copyright (C) Telecom ParisTech
# 
# This file must be used under the terms of the CeCILL. This source
# file is licensed as described in the file COPYING, which you should
# have received as part of this distribution. The terms are also
# available at:
# http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.txt
#

MandatoryFilesList="team.txt ta.py p.c"

for f in $MandatoryFilesList; do
	if [ ! -f $f ]; then
		echo "\
$f file not found. Exiting..."
		exit -1
	fi
done
if [ -f report.pdf ]; then
	report=report.pdf
else [ -f report.txt ]; then
	report=report.txt
else
	echo "\
Report not found. Please write your report, name it report.txt (plain text
report) or report.pdf (PDF format) and run the script again. Note: if both
formats are found, only the PDF version will be used. Exiting..."
	exit -1
fi

. ./team.txt

if [ -z "$student1" ]; then
	echo "\
Student #1 cannot be undefined. Please edit the team.txt file and specify at
least student1. Exiting..."
	exit -1
fi

if [ -f HWSec-TA-PY.tgz ]; then
	echo "\
HWSec-TA-PY.tgz archive already exists. Rename or delete it first. Exiting..."
	exit -1
fi

tar zcf HWSec-TA-PY.tgz "$report" $MandatoryFilesList

echo "\
################################################################################
Please check the following information and, if they are correct, send an
un-signed and un-encrypted e-mail to:
  renaud.pacalet@telecom-paristech.fr
with subject exactly:
  HWSec-TA-PY
and with the generated HWSec-TA-PY.tgz archive attached. The e-mail body and
other attachments will be ignored.
################################################################################
Student #1: $student1"
if [ -z "$email1" ]; then
	echo "\
Email #1:   none (results will be e-mailed to sender)"
else
	echo "\
Email #1:   $email1"
fi
if [ ! -z "$student2" ]; then
	echo "\
Student #2: $student2"
	if [ -z "$email2" ]; then
		echo "\
Email #2:   none"
	else
		echo "\
Email #2:   $email2"
	fi
fi
echo "\
Report:     $report"
