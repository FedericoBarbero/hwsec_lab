#
# Copyright (C) Telecom ParisTech
# 
# This file must be used under the terms of the CeCILL. This source
# file is licensed as described in the file COPYING, which you should
# have received as part of this distribution. The terms are also
# available at:
# http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.txt
#

CC		:= gcc
CFLAGS		:= -Wall -c
OPT		:= -O3
INCLUDES	:= -I./include
LD		:= gcc
LDFLAGS		:=
LIBS		:= -lm

OBJS		:= $(patsubst %.c,%.o,$(wildcard src/*.c)) src/p.o src/ta.o
EXECS		:= ta ta_acquisition
DATA		:= ta.dat ta.key

.PHONY: help all clean ultraclean

define HELP_message
Type:
  <make> or <make help> to get this help message
  <make all> to build everything
  <make clean> to clean a bit
  <make ultraclean> to really clean
endef
export HELP_message

help:
	@echo "$$HELP_message"

all: $(EXECS)

ta_acquisition: src/ta_acquisition.o src/des.o src/utils.o src/rdtsc_timer.o src/p.o
	$(LD) $(LDFLAGS) $^ -o $@ $(LIBS)

ta: src/ta.o src/des.o src/km.o src/utils.o src/pcc.o
	$(LD) $(LDFLAGS) $^ -o $@ $(LIBS)

src/ta.o: ta.c
	$(CC) $(CFLAGS) $(OPT) $(INCLUDES) $< -o $@

src/p.o: p.c
	$(CC) $(CFLAGS) -O0 $(INCLUDES) $< -o $@

%.o: %.c
	$(CC) $(CFLAGS) $(OPT) $(INCLUDES) $< -o $@

clean:
	rm -f $(OBJS)

ultraclean: clean
	rm -f $(EXECS) $(DATA)
